#!/usr/bin/ruby
#
# hikisetup - setup hiki for debian systems
# Copyright (C) Taku YASUI <tach@debian.or.jp>
#

require 'getoptlong'
require 'fileutils'

include FileUtils

HIKIDIR = '/usr/share/hiki'

def parse_options
  options = Hash.new
  parser = GetoptLong.new
  parser.set_options(['--help', '-h', GetoptLong::NO_ARGUMENT],
                     ['--lang', '-l', GetoptLong::REQUIRED_ARGUMENT])
  parser.each_option { |name, arg|  options[name.sub(/^--/, "")] = arg }
  if ( ! options['lang'] )
     options['lang'] = 'en'
  elsif ( ! options['lang'].match(/^(#{get_languages.join('|')})$/) )
    STDERR.puts("Argument of --lang must be one of following: #{get_languages.join(', ')}")
    exit 1
  end
  show_usage if options['help']
  return options
end

def input_data(default, var='variable')
  print "Please input #{var} [#{default}]: "
  ret = STDIN.gets.chop
  ret = default if ret.match(/^\s*$/)
  return ret
end

def get_languages
  Dir.glob(File.join(HIKIDIR, "messages/*.rb")).map{ |s| s.scan(%r!([^/]+)\.rb$!).first }
end

def check_datapath
  if @data_path
  elsif $data_path
    @data_path = $data_path
  else
    @data_path = File.join @hikidir, 'data'
  end
  @data_path = input_data(@data_path, 'data_path')
end

def check_pluginpath
  @plugin_path = '/usr/share/hiki/plugin' if ! @plugin_path
end

def check_templatepath
  @template_path = '/usr/share/hiki/template' if ! @template_path
end

def create_hikiconf
  if $data_path && File.exist?('hikiconf.rb')
    oldconf = 'hikiconf.rb.' + Time.now.strftime('%Y%m%d-%H%M%S')
    mv 'hikiconf.rb', oldconf
    puts "WARN: Old hikiconf.rb format found and converted to new one"
    puts "WARN: Please read /usr/share/doc/hiki/VERSIONUP.txt"
    puts "INFO: hikiconf.rb was moved to #{oldconf}"
    puts "INFO: Please remove #{oldconf} if it will be not nesessory"
  end
  if ! File.exist?('hikiconf.rb')
    begin
      cp('/usr/share/doc/hiki/examples/hikiconf.rb.sample.' + @opts['lang'] , 'hikiconf.rb')
    rescue
      STDERR.puts("Sample hikiconf.rb of \"#{@opts['lang']}\" does not found.  Using \"en\" one.")
      cp('/usr/share/doc/hiki/examples/hikiconf.rb.sample.en' , 'hikiconf.rb')
    end
  end
  conf=<<_EOT
### BEGIN: CREATED BY DEBIAN HIKISETUP ###
@data_path	= '#{@data_path}'
@plugin_path	= '#{@plugin_path}'
@template_path	= '#{@template_path}'
@cgi_name	= '#{@cgi_name}'
@lang           = '#{@opts["lang"]}'
### END: CREATED BY DEBIAN HIKISETUP ###
_EOT
  hikiconf = File.read('hikiconf.rb')
  if ! hikiconf.sub!(/### BEGIN: CREATED BY DEBIAN HIKISETUP ###.*### END: CREATED BY DEBIAN HIKISETUP ###\s*\n/m, conf)
    hikiconf += conf
  end
  File.open('hikiconf.rb', 'w') do |f|
    f.print hikiconf
  end
end

def create_hikicgi
  if ! @cgi_name
    @cgi_name = $cgi_name || 'hiki.cgi'
  end
  @cgi_name = File.basename(@cgi_name).sub(/^\.*/, '')
  @cgi_name = 'hiki.cgi' if @cgi_name == ''
  if File.exist?(@cgi_name)
    oldcgi = @cgi_name + '.' + Time.now.strftime('%Y%m%d-%H%M%S')
    mv @cgi_name, oldcgi
    puts "INFO: #{@cgi_name} was moved to #{oldcgi}"
    puts "INFO: Please remove #{oldcgi} if it will be not nesessory"
  end
  File.open(@cgi_name, 'w') do |f|
    f.print <<_EOT
#!/usr/bin/ruby -I#{HIKIDIR}
load '#{HIKIDIR}/hiki.cgi'
_EOT
  end
  chmod 0755, @cgi_name
end

def create_datadir
  if ( ! File.directory? @data_path )
    mkdir_p @data_path
    begin
      cp_r "#{HIKIDIR}/misc/text/default.#{@opts['lang']}", @data_path + "/text"
    rescue
      STDERR.puts("Default text data of \"#{@opts['lang']}\" does not found.  Using \"en\" one.")
      cp_r "#{HIKIDIR}/misc/text/default.en", @data_path + "/text"
    end
  end
end

def create_theme
  if ! File.exist? "#{@hikidir}/theme"
    cp_r "#{HIKIDIR}/theme", @hikidir
  end
end

def create_dothtaccess
  if ( @first && ! File.exist?("#{@hikidir}/.htaccess") )
    cp "/usr/share/doc/hiki/examples/dot.htaccess", ".htaccess"
  end
end

def show_usage
  print <<_EOT
Usage: hikisetup [OPTIONS] [directory]
OPTIONS:
  --help,              -h: Show this help
  --lang LANGUAGE,     -l: Specify config language (default: en)

LANGUAGE must be one of following: #{get_languages.join(", ")}
_EOT
  exit 1
end

def main
  begin
    @opts = parse_options
    @first = false
    @hikidir = File.expand_path(ARGV[0] || '.')
    chdir @hikidir
    puts "INFO: hiki basedir is #{@hikidir}"
    begin
      require 'hikiconf'
    rescue LoadError
      @first = true
    end
    check_datapath
    check_pluginpath
    check_templatepath
    makedirs(@hikidir)
    create_hikicgi
    create_theme
    create_datadir
    create_hikiconf
    create_dothtaccess
    puts "hikisetup succeeded!"
    puts "Please edit #{@hikidir}/hikiconf.rb"
  rescue
    puts 'Error: ', $!
    puts "hikisetup failed!"
  end
end

main if __FILE__ == $0
